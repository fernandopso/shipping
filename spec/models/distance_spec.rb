# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Distance do
  it { should validate_presence_of(:kilometers) }
  it { should validate_presence_of(:origin_id) }
  it { should validate_presence_of(:destination_id) }

  describe 'associations' do
    it 'origin and destination belongs_to City' do
      # setup
      bh = create(:bh)
      rj = create(:rj)

      # exercise
      distance = Distance.create(kilometers: 500, origin: bh, destination: rj)

      # verify
      expect(distance.origin).to eq bh
      expect(distance.destination).to eq rj
    end
  end

  describe 'scopes' do
    context '.origin_by' do
      it 'filter distance by city of origin' do
        # setup
        bh = create(:bh)
        sp = create(:sp)
        rj = create(:rj)

        Distance.create(kilometers: 1111, origin: bh, destination: sp)
        Distance.create(kilometers: 2222, origin: bh, destination: rj)

        # exercise
        distances = Distance.origin_by(bh)

        # verify
        expect(distances.map(&:origin).pluck(:name)).to eq %w[BH BH]
      end
    end

    context '.order_by_kilometers' do
      it 'order by kilometers' do
        # setup
        bh = create(:bh)
        sp = create(:sp)
        rj = create(:rj)

        Distance.create(kilometers: 2222, origin: bh, destination: rj)
        Distance.create(kilometers: 1111, origin: bh, destination: sp)

        # exercise
        distances = Distance.order_by_kilometers

        expect(distances.pluck(:kilometers)).to eq [1111, 2222]
      end
    end
  end

  describe '.create_or_update' do
    context 'when distance between cities exists' do
      it 'should create a distance' do
        # setup
        kilometers  = 1000
        origin      = 'BH'
        destination = 'RJ'

        # exercise
        Distance.create_or_update(kilometers, origin, destination)

        # verify
        expect(Distance.first.kilometers).to eq kilometers
        expect(Distance.first.origin.name).to eq origin
        expect(Distance.first.destination.name).to eq destination
      end
    end

    context 'when distance between cities dont exists' do
      it 'should update a distance' do
        # setup
        distance    = create(:distance_between_bh_and_rj, kilometers: 1)
        kilometers  = 500
        origin      = distance.origin.name
        destination = distance.destination.name

        # exercise
        Distance.create_or_update(kilometers, origin, destination)

        # verify
        expect(Distance.first.kilometers).to eq kilometers
        expect(Distance.first.origin.name).to eq origin
        expect(Distance.first.destination.name).to eq destination
      end
    end
  end
end
