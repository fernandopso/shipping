# frozen_string_literal: true

require 'rails_helper'

RSpec.describe City do
  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name) }

  describe '.find_or_create' do
    context 'when city dont exits' do
      it 'create city' do
        # setup
        city_name = 'BH'

        # exercise
        city = City.find_or_create(city_name)

        # exercise
        expect(city.name).to eq city_name
      end
    end

    context 'when city exits' do
      it 'find city' do
        # setup
        bh = create(:bh)

        # exercise
        city = City.find_or_create(bh.name)

        # exercise
        expect(city).to eq bh
      end
    end
  end
end
