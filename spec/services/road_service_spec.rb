# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RoadService do
  describe '.search' do
    context 'when the road between two cities exists' do
      it 'calculate kilometers' do
        # setup
        road = create(:distance_between_bh_and_rj)

        # exercise
        km = RoadService.new(road.origin, road.destination).shorter_distance

        # verify
        expect(km).to eq road.kilometers
      end
    end

    context 'when the road between two cities passes through other cities' do
      context 'when passes through one city' do
        it 'calculate kilometers' do
          # setup
          bh = create(:bh)
          sp = create(:sp)
          rj = create(:rj)

          Distance.create(kilometers: 1111, origin: bh, destination: sp)
          Distance.create(kilometers: 2222, origin: sp, destination: rj)

          # exercise
          km = RoadService.new(bh, rj).shorter_distance

          # verify
          expect(km).to eq 3333
        end
      end

      context 'when there are two roads' do
        it 'find the smallest road' do
          # setup
          bh = create(:bh)
          sp = create(:sp)
          rj = create(:rj)
          ba = create(:ba)

          Distance.create(kilometers: 111, origin: bh, destination: sp)
          Distance.create(kilometers: 222, origin: bh, destination: ba)
          Distance.create(kilometers: 333, origin: sp, destination: rj)
          Distance.create(kilometers: 444, origin: rj, destination: ba)

          # exercise
          km = RoadService.new(bh, ba).shorter_distance

          # verify
          expect(km).to eq 222
        end
      end

      context 'when the road returns to a city already visited' do
        it 'find the smallest road' do
          # setup
          bh = create(:bh)
          rj = create(:rj)
          sp = create(:sp)
          ba = create(:ba)
          ac = create(:ac)
          df = create(:df)
          rn = create(:rn)
          sc = create(:sc)
          pb = create(:pb)

          Distance.create(kilometers: 5000, origin: bh, destination: sp)
          Distance.create(kilometers: 2000, origin: sp, destination: ba)
          Distance.create(kilometers: 3000, origin: ba, destination: df)
          Distance.create(kilometers: 3000, origin: df, destination: pb)
          Distance.create(kilometers: 2000, origin: pb, destination: rn)
          Distance.create(kilometers: 6000, origin: rn, destination: ac)
          Distance.create(kilometers: 2000, origin: rn, destination: sp)
          Distance.create(kilometers: 1000, origin: rn, destination: rj)
          Distance.create(kilometers: 2000, origin: ac, destination: sc)
          Distance.create(kilometers: 1000, origin: sc, destination: rj)

          # exercise
          km = RoadService.new(bh, rj).shorter_distance

          # verify
          expect(km).to eq 16_000
        end
      end
    end
  end
end
