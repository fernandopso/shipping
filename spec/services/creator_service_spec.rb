# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CreatorService do
  context 'when send valid attributes' do
    it 'should return a success' do
      # setup
      attributes = %w[BH RJ 500]

      # exercise
      response = CreatorService.new(attributes).create

      # verify
      expect(response).to eq created: 'Distance between BH and RJ is 500 km'
    end

    context 'when dont exist cities' do
      it 'should create cities' do
        # setup
        attributes = %w[BH RJ 500]

        # exercise
        CreatorService.new(attributes).create

        # verify
        expect(City.all.pluck(:name)).to eq %w[BH RJ]
      end

      it 'should create distante with cities' do
        # setup
        attributes = %w[BH RJ 500]

        # exercise
        CreatorService.new(attributes).create

        # verify
        expect(Distance.first.kilometers).to eq 500
        expect(Distance.first.origin.name).to eq 'BH'
        expect(Distance.first.destination.name).to eq 'RJ'
      end
    end

    context 'when a city exists' do
      it 'do not should duplicate the city' do
        # setup
        bh = create(:bh)
        attributes = [bh.name.to_s, 'RJ', '500']

        # exercise
        CreatorService.new(attributes).create

        # verify
        expect(City.where(name: bh.name).count).to eq 1
      end
    end

    context 'when cities exists' do
      it 'do not should duplicate the cities' do
        # setup
        bh = create(:bh)
        rj = create(:rj)
        attributes = [bh.name.to_s, rj.name.to_s, '500']

        # exercise
        CreatorService.new(attributes).create

        # verify
        expect(City.where(name: bh.name).count).to eq 1
        expect(City.where(name: rj.name).count).to eq 1
      end
    end
  end

  context 'when send invalid attributes' do
    context 'when it does not send all attributes' do
      it 'should return an error' do
        # setup
        attributes = %w[BH RJ]

        # exercise
        response = CreatorService.new(attributes).create

        # verify
        expect(response).to eq error: 'Format should have the format A B X'
      end
    end

    context 'when send invalid distance' do
      context 'when distance is smaller' do
        it 'should return error' do
          # setup
          attributes = %w[BH RJ 0]

          # exercise
          response = CreatorService.new(attributes).create

          # verify
          expect(response).to eq error: 'Distance must be between 1 and 100000'
        end
      end

      context 'when distance is greater' do
        it 'should return an error' do
          # setup
          attributes = %w[BH RJ 100001]

          # exercise
          response = CreatorService.new(attributes).create

          # verify
          expect(response).to eq error: 'Distance must be between 1 and 100000'
        end
      end
    end
  end
end
