# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CostService do
  describe '.search' do
    context 'when the road between two cities exists' do
      it 'calculate cost to deliver' do
        # setup
        weight = 5
        road   = create(:distance_between_bh_and_rj)

        # exercise
        cost = CostService.new(weight, road.origin, road.destination).calculate

        # verify
        expect(cost).to eq 3750
      end
    end

    context 'when the road between two cities passes through other cities' do
      context 'when passes through one city' do
        it 'calculate cost to deliver' do
          # setup
          weight = 15
          bh     = create(:bh)
          sp     = create(:sp)
          rj     = create(:rj)

          Distance.create(kilometers: 1111, origin: bh, destination: sp)
          Distance.create(kilometers: 2222, origin: sp, destination: rj)

          # exercise
          cost = CostService.new(weight, bh, rj).calculate

          # verify
          expect(cost).to eq 7499.25
        end
      end

      context 'when there are two roads' do
        it 'find and lower cost to deliver' do
          # setup
          weight = 15
          bh     = create(:bh)
          sp     = create(:sp)
          rj     = create(:rj)
          ba     = create(:ba)

          Distance.create(kilometers: 111, origin: bh, destination: sp)
          Distance.create(kilometers: 222, origin: bh, destination: ba)
          Distance.create(kilometers: 333, origin: sp, destination: rj)
          Distance.create(kilometers: 444, origin: rj, destination: ba)

          # exercise
          cost = CostService.new(weight, bh, ba).calculate

          # verify
          expect(cost).to eq 499.5
        end
      end

      context 'when the road returns to a city already visited' do
        it 'find and lower cost to deliver' do
          # setup
          weight = 3
          bh     = create(:bh)
          rj     = create(:rj)
          sp     = create(:sp)
          ba     = create(:ba)
          ac     = create(:ac)
          df     = create(:df)
          rn     = create(:rn)
          sc     = create(:sc)
          pb     = create(:pb)

          Distance.create(kilometers: 5000, origin: bh, destination: sp)
          Distance.create(kilometers: 2000, origin: sp, destination: ba)
          Distance.create(kilometers: 3000, origin: ba, destination: df)
          Distance.create(kilometers: 3000, origin: df, destination: pb)
          Distance.create(kilometers: 2000, origin: pb, destination: rn)
          Distance.create(kilometers: 6000, origin: rn, destination: ac)
          Distance.create(kilometers: 2000, origin: rn, destination: sp)
          Distance.create(kilometers: 1000, origin: rn, destination: rj)
          Distance.create(kilometers: 2000, origin: ac, destination: sc)
          Distance.create(kilometers: 1000, origin: sc, destination: rj)

          # exercise
          cost = CostService.new(weight, bh, rj).calculate

          # verify
          expect(cost).to eq 7200
        end
      end
    end
  end
end
