# frozen_string_literal: true

FactoryBot.define do
  factory :bh, class: City do
    name 'BH'
  end

  factory :rj, class: City do
    name 'RJ'
  end

  factory :sp, class: City do
    name 'SP'
  end

  factory :ba, class: City do
    name 'BA'
  end

  factory :ac, class: City do
    name 'AC'
  end

  factory :df, class: City do
    name 'DF'
  end

  factory :rn, class: City do
    name 'RN'
  end

  factory :sc, class: City do
    name 'SC'
  end

  factory :pb, class: City do
    name 'PB'
  end
end
