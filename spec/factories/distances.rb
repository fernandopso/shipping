# frozen_string_literal: true

FactoryBot.define do
  factory :distance_between_bh_and_rj, class: Distance do
    kilometers 5000
    association :origin, factory: :bh
    association :destination, factory: :rj
  end
end
