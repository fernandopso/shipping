# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CostController do
  describe 'GET /index' do
    it 'response with success' do
      # exercise
      get :index

      # verify
      expect(response).to have_http_status(200)
    end

    context 'when send valid attributes' do
      context 'when weight is interge' do
        it 'calculate cost to shipping' do
          # setup
          road   = create(:distance_between_bh_and_rj)
          params = {
            weight: 5,
            origin: road.origin.name,
            destination: road.destination.name
          }

          # exercise
          get :index, params: params

          # verify
          expect(JSON.parse(response.body)).to eq 'cost' => 3750.0
        end
      end

      context 'when weight is float' do
        it 'calculate cost to shipping' do
          # setup
          road   = create(:distance_between_bh_and_rj)
          params = {
            weight: 5.5,
            origin: road.origin.name,
            destination: road.destination.name
          }

          # exercise
          get :index, params: params

          # verify
          expect(JSON.parse(response.body)).to eq 'cost' => 4125.0
        end
      end
    end

    context 'when send invalid attributes' do
      context 'when send invalid cities' do
        context 'when request without weight' do
          it 'return alert' do
            # setup
            road   = create(:distance_between_bh_and_rj)
            params = {
              origin: road.origin.name, destination: road.destination.name
            }

            # exercise
            get :index, params: params

            # verify
            expect(JSON.parse(response.body)).to eq(
              'error' => 'You need to pass the attribute weight'
            )
          end
        end

        context 'when the weight is not of the numerical type' do
          it 'raise error' do
            # setup
            road   = create(:distance_between_bh_and_rj)
            params = {
              weight: 'A',
              origin: road.origin.name,
              destination: road.destination.name
            }

            # exercise and verify
            expect { get :index, params: params }.to raise_error
          end
        end

        context 'when request without origin' do
          it 'return alert' do
            # setup
            road   = create(:distance_between_bh_and_rj)
            params = { weight: 5.5, destination: road.destination.name }

            # exercise
            get :index, params: params

            # verify
            expect(JSON.parse(response.body)).to eq(
              'error' => 'You need to pass the attribute origin'
            )
          end
        end

        context 'when dont find origin' do
          it 'return alert' do
            # setup
            road   = create(:distance_between_bh_and_rj)
            params = {
              weight: 5.5, origin: 'SP', destination: road.destination.name
            }

            # exercise
            get :index, params: params

            # verify
            expect(JSON.parse(response.body)).to eq(
              'error' => 'City SP not found'
            )
          end
        end

        context 'when request without destination' do
          it 'return alert' do
            # setup
            road   = create(:distance_between_bh_and_rj)
            params = { weight: 5.5, origin: road.origin.name }

            # exercise
            get :index, params: params

            # verify
            expect(JSON.parse(response.body)).to eq(
              'error' => 'You need to pass the attribute destination'
            )
          end
        end

        context 'when dont find destination' do
          it 'return alert' do
            # setup
            road   = create(:distance_between_bh_and_rj)
            params = {
              weight: 5.5, origin: road.origin.name, destination: 'SP'
            }

            # exercise
            get :index, params: params

            # verify
            expect(JSON.parse(response.body)).to eq(
              'error' => 'City SP not found'
            )
          end
        end
      end
    end
  end
end
