# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DistanceController do
  describe 'POST /create' do
    context 'when send valid attributes' do
      context 'when dont exist any cities' do
        it 'should create cities' do
          # setup
          body_as_raw = 'BH RJ 500'

          # exercise
          post :create, body: body_as_raw

          # verify
          expect(City.all.pluck(:name)).to eq %w[BH RJ]
        end

        it 'should create distante' do
          # setup
          body_as_raw = 'BH RJ 5000'

          # exercise
          post :create, body: body_as_raw

          # verify
          expect(JSON.parse(response.body)).to eq(
            'created' => 'Distance between BH and RJ is 5000 km'
          )
        end

        it 'response with success' do
          # setup
          body_as_raw = 'BH RJ 5000'

          # exercise
          post :create, body: body_as_raw

          # verify
          expect(response).to have_http_status(201)
        end
      end

      context 'when a city exists' do
        it 'do not should create another city' do
          # setup
          bh = create(:bh)
          body_as_raw = "#{bh.name} RJ 10"

          # exercise
          post :create, body: body_as_raw

          # verify
          expect(City.where(name: bh.name).count).to eq 1
        end
      end
    end

    context 'when send invalid attributes' do
      it 'response with error' do
        # setup
        body_as_raw = 'BH RJ 1000000000'

        # exercise
        post :create, body: body_as_raw

        # verify
        expect(response).to have_http_status(500)
      end

      context 'when send invalid distance' do
        it 'fail and return alert' do
          # setup
          body_as_raw = 'BH RJ 0'

          # exercise
          post :create, body: body_as_raw

          # verify
          expect(JSON.parse(response.body)).to eq(
            'error' => 'Distance must be between 1 and 100000'
          )
        end

        it 'fail and return alert' do
          # setup
          body_as_raw = 'BH RJ 100001'

          # exercise
          post :create, body: body_as_raw

          # verify
          expect(JSON.parse(response.body)).to eq(
            'error' => 'Distance must be between 1 and 100000'
          )
        end
      end

      context 'when request without kilometers' do
        it 'fail and return alert' do
          # setup
          body_as_raw = 'BH RJ'

          # exercise
          post :create, body: body_as_raw

          # verify
          expect(JSON.parse(response.body)).to eq(
            'error' => 'Format should have the format A B X'
          )
        end
      end

      context 'when request without a city' do
        it 'fail and return alert' do
          # setup
          body_as_raw = 'RJ 500'

          # exercise
          post :create, body: body_as_raw

          # verify
          expect(JSON.parse(response.body)).to eq(
            'error' => 'Format should have the format A B X'
          )
        end
      end
    end
  end
end
