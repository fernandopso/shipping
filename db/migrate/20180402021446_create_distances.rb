class CreateDistances < ActiveRecord::Migration[5.0]
  def change
    create_table :distances do |t|
      t.float :kilometers
      t.integer :origin_id
      t.integer :destination_id

      t.timestamps
    end
  end
end
