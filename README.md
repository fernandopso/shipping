# Getting Started with Rails

This guide covers getting up and running this application.

## Attention

This application was built from Elementary OS 0.4.1 Loki (64-bit) b uilt on "Ubuntu 16.04.3 LTS" and Linux 4.13.0-32-generic.

## Install RVM

You can use various tools to install Ruby, such as [RBENV](https://github.com/rbenv/rbenv) or [RVM](https://rvm.io/rvm/install).

As a first step install [mpapis](https://rvm.io/authors/mpapis) [public key](https://keybase.io/mpapis) used to verify installation package to ensure [security](https://rvm.io/rvm/security).

```
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
```

Install RVM:

```
\curl -sSL https://get.rvm.io | bash
```

## Install dependencies with Bundler

Install the gem dependencies that are already mentioned in Gemfile using [bundler](http://bundler.io/).

```
gem install bundler
```

In the `shipping` folder, run:

```
bundle install
```

## Install PostgreSQL

```
sudo apt-get install postgresql postgresql-contrib libpq-dev
```

Access the PostgreSQL database with the user postgres:

```
sudo -u postgres psql
```

Set a password for the postgres user:

```
alter user postgres with password 'postgres';
```

Restart PostgresSQL

```
sudo service postgresql restart
```

### Configure the `config/database.yml` to connect with your PostgreSQL

Rename `database.yml.sample` to `database.yml` and set your username and password

### Create Database

Create the database

```
bundle exec rake db:create
```

Migrate your database

```
bundle exec rake db:migrate
```

## Run server on localhost

Run the application locally

```
bundle exec rails s
```

Open the local server:

```
http://localhost:3000
```

## Run test

```
bundle exec rspec --format documentation
```
