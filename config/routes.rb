Rails.application.routes.draw do
  resources :distance, only: :create
  resources :cost,     only: :index
end
