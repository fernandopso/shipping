# frozen_string_literal: true

class CostService
  TAX = 0.15

  attr_accessor :weight, :origin, :destination

  def initialize(weight, origin, destination)
    self.weight      = weight
    self.origin      = origin
    self.destination = destination
  end

  def calculate
    kilometers * weight * TAX
  end

  private

  def kilometers
    RoadService.new(origin, destination).shorter_distance
  end
end
