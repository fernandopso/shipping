# frozen_string_literal: true

class CreatorService
  REQUIRED_ATTRIBUTES = 3
  REQUIRED_DISTANCE   = { shorter: 1, greater: 100_000 }.freeze

  attr_accessor :km, :origin, :destination

  def initialize(options)
    self.origin      = options[0]
    self.destination = options[1]
    self.km          = options[2]
  end

  def create
    return required_attributes if invalid_attributes?
    return required_distance   if invalid_distance?
    return success             if create_or_update_distance
  end

  private

  def create_or_update_distance
    Distance.create_or_update(km, origin, destination)
  end

  def required_attributes
    { error: I18n.t('invalid_attributes') }
  end

  def required_distance
    { error: I18n.t('invalid_distance', REQUIRED_DISTANCE) }
  end

  def success
    { created: I18n.t('created', km: km, initial: origin, final: destination) }
  end

  def invalid_attributes?
    origin.nil? || destination.nil? || km.nil?
  end

  def invalid_distance?
    !km.to_i.between?(*REQUIRED_DISTANCE.values)
  end
end
