# frozen_string_literal: true

class RoadService
  attr_accessor :origin, :destination, :distance, :kilometers

  def initialize(origin, destination)
    self.origin      = origin
    self.destination = destination
    self.distance    = []
    self.kilometers  = []
  end

  def shorter_distance
    discovery_roads
    kilometers.min
  end

  private

  def discovery_roads(city = origin)
    roads_for(city).each do |road|
      update_path_for(road.kilometers)

      save_road if found_destination?(road.destination)

      discovery_roads(road.destination) if allowed_distance?

      back_to_previous_city
    end
  end

  def update_path_for(kilometers)
    distance.push(kilometers)
  end

  def back_to_previous_city
    distance.pop
  end

  def save_road
    kilometers.push(distance.sum) if allowed_distance?
  end

  def roads_for(city)
    Distance.origin_by(city).order_by_kilometers
  end

  def found_destination?(city)
    city.name == destination.name
  end

  def allowed_distance?
    distance.sum <= CreatorService::REQUIRED_DISTANCE[:greater]
  end
end
