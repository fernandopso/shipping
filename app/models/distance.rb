# frozen_string_literal: true

class Distance < ApplicationRecord
  validates :kilometers,     presence: true
  validates :origin_id,      presence: true
  validates :destination_id, presence: true

  belongs_to :origin,      class_name: City
  belongs_to :destination, class_name: City

  scope :origin_by, ->(city) { where(origin: city) }
  scope :order_by_kilometers, -> { order(kilometers: :asc) }

  class << self
    def create_or_update(kilometers, origin, destination)
      find_or_build_for(origin, destination).update(kilometers: kilometers)
    end

    private

    def find_or_build_for(origin, destination)
      find_or_initialize_by(
        origin:      City.find_or_create(origin),
        destination: City.find_or_create(destination)
      )
    end
  end
end
