# frozen_string_literal: true

class City < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true

  def self.find_or_create(name)
    find_by_name(name) || create(name: name)
  end
end
