# frozen_string_literal: true

class CostController < ApplicationController
  before_action :require_attributes, only: :index
  before_action :set_cities,         only: :index
  before_action :require_cities,     only: :index

  def index
    render json: { cost: cost_to_shipping }
  end

  private

  def cost_to_shipping
    CostService.new(Float(weight), @origin, @destination).calculate
  end

  def require_attributes
    return render(json: { error: alert(:weight) }) if weight.nil?
    return render(json: { error: alert(:origin) }) if origin.nil?
    return render(json: { error: alert(:destination) }) if destination.nil?
  end

  def set_cities
    @origin      = City.find_by_name(origin)
    @destination = City.find_by_name(destination)
  end

  def require_cities
    return render(json: { error: city_alert(origin) }) if @origin.nil?
    return render(json: { error: city_alert(destination) }) if @destination.nil?
  end

  %i[weight origin destination].each do |attribute|
    define_method attribute do
      params[attribute]
    end
  end

  def city_alert(city)
    t('city_not_found', city: city)
  end

  def alert(attribute)
    t('require_attribute', attribute: attribute)
  end
end
