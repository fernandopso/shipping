# frozen_string_literal: true

class DistanceController < ApplicationController
  ATTRIBUTE_SEPARATOR = ' '

  before_action :create_service, only: :create

  def create
    render json: @distance, status: @distance.keys.first
  end

  private

  def create_service
    @distance = CreatorService.new(attributes).create
  end

  def attributes
    request.raw_post.split(ATTRIBUTE_SEPARATOR).uniq
  end
end
